const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

require('dotenv').config();

const mongodbString = `mongodb://TodoListApp:${process.env.MONGODB_PASSWORD}@todoapp-shard-00-00-hrtn3.mongodb.net:27017,todoapp-shard-00-01-hrtn3.mongodb.net:27017,todoapp-shard-00-02-hrtn3.mongodb.net:27017/test?ssl=true&replicaSet=TodoApp-shard-0&authSource=admin&retryWrites=true`;

const taskRouter = require('./api/routes/tasks');

mongoose.connect(
  mongodbString,
  {
    useNewUrlParser: true
  }
);

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  if(req.method==='OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    return res.status(200).json({});
  }

  next();
});

app.use('/tasks', taskRouter);

app.use((req,res,next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
}); 

module.exports = app;