const mongoose = require('mongoose');

const taskSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: String,
  description: String,
  createdAt: String
});

module.exports = mongoose.model('Task', taskSchema);