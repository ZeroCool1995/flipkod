<?php

  require( 'classes/GeometrijskiLik.php' );
  require( 'classes/Trokut.php' );
  require( 'classes/Krug.php' );

  $trokut = new Trokut(5,5,5,5*sqrt(2));
  $krug = new Krug(5);

  echo "Povrsina trokuta: ".$trokut->getPovrsina()."<br />";
  echo "Opseg trokuta: ".$trokut->getOpseg()."<br />";
  echo "Povrsina kruga: ".$krug->getPovrsina()."<br />";
  echo "Opseg kruga: ".$krug->getOpseg()."<br />";
?>