<?php

  class Krug extends GeometrijskiLik {

    const PI = 3.14;
    private $r;

    function __construct( $_r ) { $this->r = $_r; }

    public function getOpseg() { return 2*$this->r*self::PI; }
    public function getPovrsina() { return $this->r*self::PI*self::PI; }
  }

?>