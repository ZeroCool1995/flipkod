<?php

  abstract class GeometrijskiLik {

    abstract public function getPovrsina();
    abstract public function getOpseg();
  }

?>