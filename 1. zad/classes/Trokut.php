<?php

  class Trokut extends GeometrijskiLik {

    private $a, $b, $c, $va;

    function __construct( $_a, $_b, $_c, $_va ) {

      $this->a = $_a;
      $this->b = $_b;
      $this->c = $_c;
      $this->va = $_va;
    }

    public function getOpseg() { return $this->a+$this->b+$this->c; }
    public function getPovrsina() { return 0.5*$this->a*$this->va; }
  }

?>